package com.soft.yamdb.constraint.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.soft.yamdb.constraint.CurrentPassword;
import com.soft.yamdb.entity.User;
import com.soft.yamdb.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

public class CurrentPasswordValidator implements ConstraintValidator<CurrentPassword, String> {
    @Autowired
    private UserService userService;

    @Override
    public boolean isValid(String plainPassword, ConstraintValidatorContext context) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userService.isUserPassword(plainPassword, user);
    }

}
