package com.soft.yamdb.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.soft.yamdb.constraint.CurrentPassword;
import com.soft.yamdb.constraint.FieldsValueMatch;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@FieldsValueMatch(field = "newPlainPassword", fieldMatch = "newPlainPasswordRepeated", message = "New password fields do not match.")
public class AccountPasswordChangeFormDto {
    @CurrentPassword
    private String currentPlainPassword;
    @Pattern(regexp = "[^\\s]+", message = "New password must not contain whitespace characters.")
    @Size(min = 5, message = "New password must contain at least 5 characters.")
    private String newPlainPassword;
    @Pattern(regexp = "[^\\s]+", message = "New password must not contain whitespace characters.")
    @Size(min = 5, message = "New password must contain at least 5 characters.")
    private String newPlainPasswordRepeated;
}
