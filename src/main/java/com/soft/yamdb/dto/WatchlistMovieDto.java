package com.soft.yamdb.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WatchlistMovieDto {
    private Long   movieId;
    private String title;
    private String poster;
    private String added;
}
