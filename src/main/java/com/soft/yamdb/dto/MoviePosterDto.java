package com.soft.yamdb.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoviePosterDto {
    private Long   id;
    private String title;
    private String poster;
}
