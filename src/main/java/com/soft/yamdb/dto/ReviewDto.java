package com.soft.yamdb.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewDto {
    private Long    movieId;
    private String  title;
    private String  content;
    private int     rating;
    private boolean containsSpoilers;
    private String  date;
    private String  author;
}
