package com.soft.yamdb.dto;

import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDeleteFormDto {
    @Pattern(regexp = "DELETE", message = "User input does not match required confirmation text.")
    private String userConfirmation;
}
