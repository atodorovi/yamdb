package com.soft.yamdb.dto;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieDto {
    private Long   id;
    private String title;
    private String poster;
    private String backdrop;
    private String releaseDate;
    private String plot;
    private String tagline;
    private String runtime;
    private String imdb;
    private String youtube;

    private List<String>              genres;
    private List<Map<String, String>> cast;
}
