package com.soft.yamdb.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.soft.yamdb.constraint.FieldsValueMatch;
import com.soft.yamdb.constraint.UniqueUsername;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@FieldsValueMatch(field = "plainPassword", fieldMatch = "plainPasswordRepeated", message = "Password fields do not match.")
public class RegisterFormDto {
    @Pattern(regexp = "[^\\s]+", message = "Username must not contain whitespace characters.")
    @Size(min = 5, max = 20, message = "Username must contain between 5 and 20 characters.")
    @UniqueUsername
    private String username;
    @Pattern(regexp = "[^\\s]+", message = "Password must not contain whitespace characters.")
    @Size(min = 5, message = "Password must contain at least 5 characters.")
    private String plainPassword;
    @Pattern(regexp = "[^\\s]+", message = "Password must not contain whitespace characters.")
    @Size(min = 5, message = "Password must contain at least 5 characters.")
    private String plainPasswordRepeated;
}
