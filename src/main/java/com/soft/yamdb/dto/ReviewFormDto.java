package com.soft.yamdb.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewFormDto {
    private Long    movieId;
    @NotBlank(message = "Title must not consist solely of whitespace characters.")
    @Size(min = 3, max = 50, message = "Title must contain between 3 and 50 characters.")
    private String  title;
    @NotBlank(message = "Content must not consist solely of whitespace characters.")
    @Size(min = 100, max = 2000, message = "Content must contain between 100 and 2000 characters.")
    private String  content;
    @Min(value = 1, message = "Review rating can not be less than 1.")
    @Max(value = 10, message = "Review rating can not be greater than 10.")
    private int     rating;
    private boolean containsSpoilers;
}
