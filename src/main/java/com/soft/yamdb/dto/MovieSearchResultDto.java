package com.soft.yamdb.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieSearchResultDto {
    private Long   id;
    private String title;
    private String poster;
    private String year;
    private String plot;
}
