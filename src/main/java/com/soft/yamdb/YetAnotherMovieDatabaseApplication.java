package com.soft.yamdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class YetAnotherMovieDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(YetAnotherMovieDatabaseApplication.class, args);
    }

}
