package com.soft.yamdb.mapper;

import com.soft.yamdb.dto.ReviewDto;
import com.soft.yamdb.entity.Review;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class ReviewMapper {
    @Mapping(source = "spoilers", target = "containsSpoilers")
    @Mapping(source = "creationDate", target = "date", dateFormat = "dd.MM.yyyy.")
    @Mapping(source = "user.username", target = "author")
    public abstract ReviewDto toReviewDto(Review review);
}
