package com.soft.yamdb.mapper;

import com.soft.yamdb.dto.RegisterFormDto;
import com.soft.yamdb.dto.ReviewFormDto;
import com.soft.yamdb.entity.Review;
import com.soft.yamdb.entity.User;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class FormMapper {
    @Mapping(source = "username", target = "username")
    @Mapping(source = "plainPassword", target = "password")
    public abstract User toUser(RegisterFormDto registerFormDto);

    @Mapping(source = "containsSpoilers", target = "spoilers")
    public abstract Review toReview(ReviewFormDto reviewFormDto);
}
