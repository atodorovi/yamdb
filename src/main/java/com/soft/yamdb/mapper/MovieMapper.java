package com.soft.yamdb.mapper;

import com.soft.yamdb.dto.MovieSearchResultDto;
import com.soft.yamdb.dto.WatchlistMovieDto;
import com.soft.yamdb.entity.WatchlistedMovie;

import java.util.List;
import java.util.Set;

import com.soft.yamdb.dto.MovieDto;
import com.soft.yamdb.dto.MoviePosterDto;
import com.soft.yamdb.response.BaseMovieResponse;
import com.soft.yamdb.response.DetailedMovieResponse;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Value;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Value("${tmdb.image.url}")
    protected String imageBaseUrl;

    @Mapping(source = "posterPath", target = "poster")
    @Mapping(source = "backdropPath", target = "backdrop")
    @Mapping(source = "overview", target = "plot")
    @Mapping(source = "releaseDate", target = "releaseDate", dateFormat = "dd.MM.yyyy.")
    @Mapping(target = "genres", expression = "java(getGenres(movieResponse))")
    public abstract MovieDto toMovieDto(DetailedMovieResponse movieResponse);

    @Mapping(source = "posterPath", target = "poster")
    @Mapping(source = "overview", target = "plot")
    @Mapping(source = "releaseDate", target = "year", dateFormat = "yyyy")
    public abstract MovieSearchResultDto toMovieSearchResultDto(BaseMovieResponse movieResponse);

    @Mapping(source = "posterPath", target = "poster")
    public abstract MoviePosterDto toMoviePosterDto(BaseMovieResponse movieResponse);

    @Mapping(source = "movieTitle", target = "title")
    public abstract WatchlistMovieDto toWatchlistMovieDto(WatchlistedMovie watchlistedMovie);

    protected final List<String> getGenres(DetailedMovieResponse movieResponse) {
        return movieResponse.getGenres().stream().map(map -> map.get("name")).toList();
    }

    @AfterMapping
    protected final void setAddedField(WatchlistedMovie watchlistedMovie,
            @MappingTarget WatchlistMovieDto watchlistMovieDto) {
        watchlistMovieDto.setAdded(Long.toString(watchlistedMovie.getCreationDate().toInstant().toEpochMilli()));
    }

    @AfterMapping
    protected final void setPosterUrl(WatchlistedMovie watchlistedMovie,
            @MappingTarget WatchlistMovieDto watchlistMovieDto) {
        if (watchlistedMovie.getPosterPath().isEmpty()) {
            String fullUrl = "https://fakeimg.pl/342x513/?font_size=52&text=" + watchlistMovieDto.getTitle();
            watchlistMovieDto.setPoster(fullUrl);
        }
        else {
            watchlistMovieDto.setPoster(watchlistedMovie.getPosterPath());
        }
    }

    @AfterMapping
    protected final void setSearchResultTitle(@MappingTarget MovieSearchResultDto movieSearchResultDto) {
        if (movieSearchResultDto.getYear() != null) {
            String title = movieSearchResultDto.getTitle() + " (" + movieSearchResultDto.getYear() + ")";
            movieSearchResultDto.setTitle(title);
        }
    }

    @AfterMapping
    protected final void setSmallSizePosterUrl(@MappingTarget MovieSearchResultDto movieSearchResultDto) {
        if (movieSearchResultDto.getPoster() != null) {
            String fullUrl = imageBaseUrl + "/w92" + movieSearchResultDto.getPoster();
            movieSearchResultDto.setPoster(fullUrl);
        }
    }

    @AfterMapping
    protected final void setMediumSizePosterUrl(@MappingTarget MoviePosterDto moviePosterDto) {
        if (moviePosterDto.getPoster() != null) {
            String fullUrl = imageBaseUrl + "/w342" + moviePosterDto.getPoster();
            moviePosterDto.setPoster(fullUrl);
        }
    }

    @AfterMapping
    protected final void setBigSizePosterUrl(@MappingTarget MovieDto movieDto) {
        if (movieDto.getPoster() != null) {
            String fullUrl = imageBaseUrl + "/w500" + movieDto.getPoster();
            movieDto.setPoster(fullUrl);
        }
    }

    @AfterMapping
    protected final void setBackdropUrl(@MappingTarget MovieDto movieDto) {
        String fullUrl = imageBaseUrl + "/w1280" + movieDto.getBackdrop();
        movieDto.setBackdrop(fullUrl);
    }

    @AfterMapping
    protected final void setRuntime(DetailedMovieResponse movieResponse, @MappingTarget MovieDto movieDto) {
        int hours   = movieResponse.getRuntime() / 60;
        int minutes = movieResponse.getRuntime() % 60;

        if (hours > 0) {
            movieDto.setRuntime(hours + "h " + minutes + "m");
        }
        else {
            movieDto.setRuntime(minutes + "m");
        }
    }

    @AfterMapping
    protected final void setImdb(DetailedMovieResponse movieResponse, @MappingTarget MovieDto movieDto) {
        movieDto.setImdb("https://www.imdb.com/title/" + movieResponse.getImdbId());
    }

    @AfterMapping
    protected final void setCast(DetailedMovieResponse movieResponse, @MappingTarget MovieDto movieDto) {
        // create a new key in each map that points to actor's image
        movieResponse.getCreditsResponse().getCast().forEach(map -> map.put("photo",
                map.get("profile_path") != null ? imageBaseUrl + "/w185" + map.get("profile_path") : null));
        // remove keys from each map that are not in the provided set
        movieResponse.getCreditsResponse().getCast()
                     .forEach(map -> map.keySet().retainAll(Set.of("name", "character", "photo")));
        movieDto.setCast(movieResponse.getCreditsResponse().getCast());
    }

    @AfterMapping
    protected final void setYoutube(DetailedMovieResponse movieResponse, @MappingTarget MovieDto movieDto) {
        String youtubeId = movieResponse.getVideosResponse()
                                        .getVideos()
                                        .stream()
                                        .filter(map -> map.get("site").equalsIgnoreCase("Youtube"))
                                        .filter(map -> map.get("type").equalsIgnoreCase("Trailer"))
                                        .map(map -> map.get("key"))
                                        .findFirst()
                                        .orElse(null);

        if (youtubeId != null) {
            movieDto.setYoutube("https://www.youtube.com/watch?v=" + youtubeId);
        }
        else {
            movieDto.setYoutube(null);
        }
    }
}
