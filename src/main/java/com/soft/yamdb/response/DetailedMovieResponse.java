package com.soft.yamdb.response;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.lang.Nullable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DetailedMovieResponse extends BaseMovieResponse {
    @JsonProperty(value = "backdrop_path")
    private String  backdropPath;
    @Nullable
    @JsonProperty(value = "imdb_id")
    private String  imdbId;
    @Nullable
    private Integer runtime;
    @Nullable
    private String  tagline;

    private List<Map<String, String>> genres;

    @JsonProperty(value = "credits")
    private CreditsResponse creditsResponse;
    @JsonProperty(value = "videos")
    private VideosResponse  videosResponse;

    @Data
    public class CreditsResponse {
        @JsonProperty(value = "cast")
        private List<Map<String, String>> cast;
        @JsonProperty(value = "crew")
        private List<Map<String, String>> crew;
    }

    @Data
    public class VideosResponse {
        @JsonProperty(value = "results")
        private List<Map<String, String>> videos;
    }
}
