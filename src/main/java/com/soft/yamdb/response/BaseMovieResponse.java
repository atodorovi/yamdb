package com.soft.yamdb.response;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseMovieResponse {
    @Nullable
    @JsonProperty(value = "poster_path")
    private String posterPath;
    private String overview;
    @JsonProperty(value = "release_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date   releaseDate;
    private Long   id;
    private String title;
    @Nullable
    @JsonProperty(value = "backdrop_path")
    private String backdropPath;
    private double popularity;
}
