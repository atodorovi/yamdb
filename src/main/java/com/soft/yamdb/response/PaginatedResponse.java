package com.soft.yamdb.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PaginatedResponse<T> {
    private int page;
    @JsonProperty(value = "total_pages")
    private int totalPages;
    @JsonProperty(value = "total_results")
    private int totalResults;

    private List<T> results;
}
