package com.soft.yamdb.repository;

import java.util.List;

import com.soft.yamdb.entity.User;
import com.soft.yamdb.entity.WatchlistedMovie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface WatchlistedMovieRepository extends JpaRepository<WatchlistedMovie, Long> {
    boolean existsByUserAndMovieId(User user, Long movieId);
    Long deleteByUserAndMovieId(User user, Long movieId);
    List<WatchlistedMovie> findByUser(User user);
    Long deleteWatchlistedMovieByUser(User user);
}
