package com.soft.yamdb.repository;

import java.util.List;

import com.soft.yamdb.entity.Review;
import com.soft.yamdb.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ReviewRepository extends JpaRepository<Review, Long> {
    Review findByUserAndMovieId(User user, Long movieId);
    boolean existsByUserAndMovieId(User user, Long movieId);
    Review findFirstByMovieIdOrderByCreationDateDesc(Long movieId);
    long countByMovieId(Long movieId);
    List<Review> findByMovieId(Long movieId);
    Long deleteReviewByUser(User user);
}
