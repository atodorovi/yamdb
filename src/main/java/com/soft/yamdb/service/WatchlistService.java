package com.soft.yamdb.service;

import java.util.List;

import com.soft.yamdb.entity.User;
import com.soft.yamdb.entity.WatchlistedMovie;

public interface WatchlistService {
    WatchlistedMovie addMovieToUserWatchlist(User user, String movieId, String movieTitle, String posterPath);
    Long removeMovieFromUserWatchlist(User user, String movieId);
    boolean isMovieOnUserWatchlist(User user, String movieId);
    List<WatchlistedMovie> getUserWatchlist(User user);
    void deleteUserWatchlist(User user);
}
