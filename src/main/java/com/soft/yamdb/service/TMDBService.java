package com.soft.yamdb.service;

import java.util.List;

import com.soft.yamdb.response.BaseMovieResponse;
import com.soft.yamdb.response.DetailedMovieResponse;

public interface TMDBService {
    List<BaseMovieResponse> getPopularMovies();
    List<BaseMovieResponse> searchMovieByTitle(String title);
    DetailedMovieResponse getMovieDetails(String id);
}
