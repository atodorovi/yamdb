package com.soft.yamdb.service;

import java.util.List;

import com.soft.yamdb.entity.Review;
import com.soft.yamdb.entity.User;

public interface ReviewService {
    Review addReviewToMovie(Review review, User user, String movieId);
    Review getUserReview(User user, String movieId);
    Review getLatestReview(String movieId);
    long getNumberOfReviews(String movieId);
    boolean isMovieReviewedByUser(User user, String movieId);
    List<Review> getReviews(String movieId);
    void deleteUserReviews(User user);
}
