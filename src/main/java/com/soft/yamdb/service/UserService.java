package com.soft.yamdb.service;

import com.soft.yamdb.entity.User;

public interface UserService {
    boolean isUsernameTaken(String username);
    boolean isUserPassword(String plainPassword, User user);
    void changeUserPassword(String newPlainPassword, User user);
    User createUser(User user);
    void deleteUserAndAllUserContent(User user);
}
