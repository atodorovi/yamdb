package com.soft.yamdb.service.impl;

import java.util.List;

import com.soft.yamdb.feign.TMDBClient;
import com.soft.yamdb.response.BaseMovieResponse;
import com.soft.yamdb.response.DetailedMovieResponse;
import com.soft.yamdb.service.TMDBService;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class TMDBServiceImpl implements TMDBService {
    private TMDBClient tmdbClient;

    @Override
    public List<BaseMovieResponse> getPopularMovies() {
        List<BaseMovieResponse> popularMovies = tmdbClient.getPopularMovies().getResults();
        popularMovies.sort((a, b) -> Double.compare(b.getPopularity(), a.getPopularity())); // movies are not correctly sorted by popularity
        return popularMovies;
    }

    @Override
    public List<BaseMovieResponse> searchMovieByTitle(String title) {
        return tmdbClient.searchMovieByTitle(title).getResults();
    }

    @Override
    public DetailedMovieResponse getMovieDetails(String id) {
        return tmdbClient.getMovieDetails(id);
    }
}
