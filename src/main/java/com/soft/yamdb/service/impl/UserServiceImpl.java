package com.soft.yamdb.service.impl;

import com.soft.yamdb.entity.User;
import com.soft.yamdb.repository.UserRepository;
import com.soft.yamdb.service.ReviewService;
import com.soft.yamdb.service.UserService;
import com.soft.yamdb.service.WatchlistService;

import java.util.Optional;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {
    private UserRepository   userRepository;
    private PasswordEncoder  passwordEncoder;
    private ReviewService    reviewService;
    private WatchlistService watchlistService;

    @Override
    public boolean isUsernameTaken(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        return user.isPresent();
    }

    @Override
    public boolean isUserPassword(String plainPassword, User user) {
        return passwordEncoder.matches(plainPassword, user.getPassword());
    }

    @Override
    public void changeUserPassword(String newPlainPassword, User user) {
        user.setPassword(passwordEncoder.encode(newPlainPassword));
        userRepository.save(user);
    }

    @Override
    public User createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public void deleteUserAndAllUserContent(User user) {
        reviewService.deleteUserReviews(user);
        watchlistService.deleteUserWatchlist(user);
        userRepository.delete(user);
    }

}
