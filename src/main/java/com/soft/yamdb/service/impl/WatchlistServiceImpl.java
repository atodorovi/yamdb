package com.soft.yamdb.service.impl;

import java.util.List;

import com.soft.yamdb.entity.User;
import com.soft.yamdb.entity.WatchlistedMovie;
import com.soft.yamdb.repository.WatchlistedMovieRepository;
import com.soft.yamdb.service.WatchlistService;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class WatchlistServiceImpl implements WatchlistService {
    private WatchlistedMovieRepository watchlistedMovieRepository;

    @Override
    public WatchlistedMovie addMovieToUserWatchlist(User user, String movieId, String movieTitle, String posterPath) {
        WatchlistedMovie movie = new WatchlistedMovie();
        movie.setUser(user);
        movie.setMovieId(Long.parseLong(movieId));
        movie.setMovieTitle(movieTitle);
        movie.setPosterPath(posterPath);
        return watchlistedMovieRepository.save(movie);
    }

    @Override
    public Long removeMovieFromUserWatchlist(User user, String movieId) {
        return watchlistedMovieRepository.deleteByUserAndMovieId(user, Long.parseLong(movieId));
    }

    @Override
    public boolean isMovieOnUserWatchlist(User user, String movieId) {
        return watchlistedMovieRepository.existsByUserAndMovieId(user, Long.parseLong(movieId));
    }

    @Override
    public List<WatchlistedMovie> getUserWatchlist(User user) {
        return watchlistedMovieRepository.findByUser(user);
    }

    @Override
    public void deleteUserWatchlist(User user) {
        watchlistedMovieRepository.deleteWatchlistedMovieByUser(user);
    }
}
