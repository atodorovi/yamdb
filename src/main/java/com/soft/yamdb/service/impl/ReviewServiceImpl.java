package com.soft.yamdb.service.impl;

import java.util.List;

import com.soft.yamdb.entity.Review;
import com.soft.yamdb.entity.User;
import com.soft.yamdb.repository.ReviewRepository;
import com.soft.yamdb.service.ReviewService;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class ReviewServiceImpl implements ReviewService {
    private ReviewRepository reviewRepository;

    @Override
    public Review addReviewToMovie(Review review, User user, String movieId) {
        review.setMovieId(Long.parseLong(movieId));
        review.setUser(user);
        return reviewRepository.save(review);
    }

    @Override
    public Review getUserReview(User user, String movieId) {
        return reviewRepository.findByUserAndMovieId(user, Long.parseLong(movieId));
    }

    @Override
    public Review getLatestReview(String movieId) {
        return reviewRepository.findFirstByMovieIdOrderByCreationDateDesc(Long.parseLong(movieId));
    }

    @Override
    public long getNumberOfReviews(String movieId) {
        return reviewRepository.countByMovieId(Long.parseLong(movieId));
    }

    @Override
    public boolean isMovieReviewedByUser(User user, String movieId) {
        return reviewRepository.existsByUserAndMovieId(user, Long.parseLong(movieId));
    }

    @Override
    public List<Review> getReviews(String movieId) {
        return reviewRepository.findByMovieId(Long.parseLong(movieId));
    }

    @Override
    public void deleteUserReviews(User user) {
        reviewRepository.deleteReviewByUser(user);
    }
}
