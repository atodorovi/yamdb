package com.soft.yamdb.feign;

import com.soft.yamdb.response.BaseMovieResponse;
import com.soft.yamdb.response.DetailedMovieResponse;
import com.soft.yamdb.response.PaginatedResponse;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "TMDB", url = "${tmdb.api.url}")
public interface TMDBClient {
    @GetMapping(path = "/movie/popular?api_key=${tmdb.api.key}")
    PaginatedResponse<BaseMovieResponse> getPopularMovies();

    @GetMapping(path = "/search/movie?api_key=${tmdb.api.key}&query={title}")
    PaginatedResponse<BaseMovieResponse> searchMovieByTitle(@PathVariable("title") String title);

    @GetMapping(path = "/movie/{id}?api_key=${tmdb.api.key}&append_to_response=credits%2Cvideos")
    DetailedMovieResponse getMovieDetails(@PathVariable("id") String id);
}
