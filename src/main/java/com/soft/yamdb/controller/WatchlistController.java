package com.soft.yamdb.controller;

import java.util.List;

import com.soft.yamdb.entity.User;
import com.soft.yamdb.entity.WatchlistedMovie;
import com.soft.yamdb.mapper.MovieMapper;
import com.soft.yamdb.service.WatchlistService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/watchlist")
@Secured("ROLE_USER")
public class WatchlistController {
    private WatchlistService watchlistService;
    private MovieMapper      movieMapper;

    @GetMapping("")
    public String getWatchlist(@AuthenticationPrincipal User user, Model model) {
        List<WatchlistedMovie> movies = watchlistService.getUserWatchlist(user);
        model.addAttribute("watchlistMoviesDto", movies.stream().map(movieMapper::toWatchlistMovieDto).toList());
        return "watchlist";
    }

    @DeleteMapping(value = "/remove/{id}", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Long> removeMovieFromWatchlist(@PathVariable("id") String movieId,
            @AuthenticationPrincipal User user) {
        Long moviesDeleted = watchlistService.removeMovieFromUserWatchlist(user, movieId);
        if (moviesDeleted == 1) {
            return new ResponseEntity<>(moviesDeleted, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(0L, HttpStatus.NOT_FOUND);
        }
    }
}
