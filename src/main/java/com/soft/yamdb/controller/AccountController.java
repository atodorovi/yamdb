package com.soft.yamdb.controller;

import javax.validation.Valid;

import com.soft.yamdb.dto.AccountDeleteFormDto;
import com.soft.yamdb.dto.AccountPasswordChangeFormDto;
import com.soft.yamdb.entity.User;
import com.soft.yamdb.service.UserService;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/account")
@Secured("ROLE_USER")
public class AccountController {
    private UserService userService;

    @GetMapping("")
    public String getAccountPanel(Model model) {
        model.addAttribute("activeTab", "password-tab");
        model.addAttribute("accountDeleteFormDto", new AccountDeleteFormDto());
        model.addAttribute("accountPasswordChangeFormDto", new AccountPasswordChangeFormDto());
        return "account-settings";
    }

    @PostMapping("/password")
    public String changeUserPassword(@Valid AccountPasswordChangeFormDto accountPasswordChangeFormDto,
            BindingResult bindingResult, @AuthenticationPrincipal User user, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("activeTab", "password-tab");
            model.addAttribute("accountDeleteFormDto", new AccountDeleteFormDto());
            model.addAttribute("accountPasswordChangeFormDto", accountPasswordChangeFormDto);
            return "account-settings";
        }
        userService.changeUserPassword(accountPasswordChangeFormDto.getNewPlainPassword(), user);
        return "redirect:/logout";
    }

    @PostMapping("/delete")
    public String deleteUserAccount(@Valid AccountDeleteFormDto accountDeleteFormDto, BindingResult bindingResult,
            @AuthenticationPrincipal User user, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("activeTab", "delete-tab");
            model.addAttribute("accountPasswordChangeFormDto", new AccountPasswordChangeFormDto());
            model.addAttribute("accountDeleteFormDto", accountDeleteFormDto);
            return "account-settings";
        }
        userService.deleteUserAndAllUserContent(user);
        return "redirect:/logout";
    }
}
