package com.soft.yamdb.controller;

import javax.validation.Valid;

import com.soft.yamdb.dto.MovieDto;
import com.soft.yamdb.dto.ReviewFormDto;
import com.soft.yamdb.entity.Review;
import com.soft.yamdb.entity.User;
import com.soft.yamdb.mapper.FormMapper;
import com.soft.yamdb.mapper.MovieMapper;
import com.soft.yamdb.mapper.ReviewMapper;
import com.soft.yamdb.response.DetailedMovieResponse;
import com.soft.yamdb.service.ReviewService;
import com.soft.yamdb.service.TMDBService;
import com.soft.yamdb.service.WatchlistService;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/movie")
public class MovieController {
    private TMDBService      tmdbService;
    private WatchlistService watchlistService;
    private ReviewService    reviewService;
    private MovieMapper      movieMapper;
    private ReviewMapper     reviewMapper;
    private FormMapper       formMapper;

    @GetMapping("/{id}")
    public String getMovie(@PathVariable("id") String movieId, @AuthenticationPrincipal User user, Model model) {
        DetailedMovieResponse movie        = tmdbService.getMovieDetails(movieId);
        MovieDto              movieDto     = movieMapper.toMovieDto(movie);
        Review                latestReview = reviewService.getLatestReview(movieId);

        if (latestReview != null) {
            latestReview.setContent(latestReview.getContent().replace("\n", "<br/>"));
        }

        model.addAttribute("movieDto", movieDto);
        model.addAttribute("onWatchlist", watchlistService.isMovieOnUserWatchlist(user, movieId));
        model.addAttribute("latestReviewDto", reviewMapper.toReviewDto(latestReview));
        model.addAttribute("reviewCount", reviewService.getNumberOfReviews(movieId));
        model.addAttribute("reviewed", reviewService.isMovieReviewedByUser(user, movieId));
        return "movie";
    }

    @Secured("ROLE_USER")
    @PostMapping("/{id}/add")
    public String addMovieToWatchlist(@PathVariable("id") String movieId, @RequestParam String movieTitle,
            @RequestParam String posterPath, @AuthenticationPrincipal User user,
            RedirectAttributes redirectAttributes) {
        watchlistService.addMovieToUserWatchlist(user, movieId, movieTitle, posterPath);
        redirectAttributes.addAttribute("id", movieId);
        return "redirect:/movie/{id}";
    }

    @Secured("ROLE_USER")
    @PostMapping("/{id}/remove")
    public String removeMovieFromWatchlist(@PathVariable("id") String movieId, @AuthenticationPrincipal User user,
            RedirectAttributes redirectAttributes) {
        watchlistService.removeMovieFromUserWatchlist(user, movieId);
        redirectAttributes.addAttribute("id", movieId);
        return "redirect:/movie/{id}";
    }

    @Secured("ROLE_USER")
    @GetMapping("/{id}/review")
    public String showReviewForm(@PathVariable("id") String movieId, @AuthenticationPrincipal User user, Model model) {
        Review review = reviewService.getUserReview(user, movieId);

        model.addAttribute("movieDto", movieMapper.toMovieDto(tmdbService.getMovieDetails(movieId)));
        if (review != null) {
            model.addAttribute("reviewDto", reviewMapper.toReviewDto(review));
            return "my-review";
        }
        else {
            model.addAttribute("reviewFormDto", new ReviewFormDto());
            return "review";
        }
    }

    @Secured("ROLE_USER")
    @PostMapping("/{id}/review")
    public String submitReview(@PathVariable("id") String movieId, @Valid ReviewFormDto reviewFormDto,
            BindingResult bindingResult, @AuthenticationPrincipal User user, Model model,
            RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("movieDto", movieMapper.toMovieDto(tmdbService.getMovieDetails(movieId)));
            model.addAttribute("reviewFormDto", reviewFormDto);
            return "review";
        }

        reviewService.addReviewToMovie(formMapper.toReview(reviewFormDto), user, movieId);
        redirectAttributes.addAttribute("id", movieId);
        return "redirect:/movie/{id}";
    }
}
