package com.soft.yamdb.controller;

import java.util.List;

import com.soft.yamdb.entity.Review;
import com.soft.yamdb.mapper.MovieMapper;
import com.soft.yamdb.mapper.ReviewMapper;
import com.soft.yamdb.response.DetailedMovieResponse;
import com.soft.yamdb.service.ReviewService;
import com.soft.yamdb.service.TMDBService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/reviews")
public class ReviewsController {
    private ReviewService reviewService;
    private TMDBService   tmdbService;
    private MovieMapper   movieMapper;
    private ReviewMapper  reviewMapper;

    @GetMapping("/{id}")
    public String getMovieReviews(@PathVariable("id") String movieId, Model model) {
        List<Review>          reviews = reviewService.getReviews(movieId);
        DetailedMovieResponse movie   = tmdbService.getMovieDetails(movieId);
        model.addAttribute("movieDto", movieMapper.toMovieDto(movie));
        model.addAttribute("reviewsDto", reviews.stream().map(reviewMapper::toReviewDto).toList());
        return "reviews";
    }
}
