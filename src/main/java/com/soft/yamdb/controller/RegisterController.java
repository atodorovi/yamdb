package com.soft.yamdb.controller;

import javax.validation.Valid;

import com.soft.yamdb.dto.RegisterFormDto;
import com.soft.yamdb.entity.User;
import com.soft.yamdb.mapper.FormMapper;
import com.soft.yamdb.service.UserService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/register")
public class RegisterController {
    private UserService userService;
    private FormMapper  formMapper;

    @GetMapping("")
    public String showRegisterForm(Model model) {
        model.addAttribute("registerFormDto", new RegisterFormDto());
        return "register";
    }

    @PostMapping("")
    public String registerUser(@Valid RegisterFormDto registerFormDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("registerFormDto", registerFormDto);
            return "register";
        }

        User newUser = formMapper.toUser(registerFormDto);
        userService.createUser(newUser);
        return "redirect:/login?success";
    }
}
