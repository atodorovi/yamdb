package com.soft.yamdb.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/login")
public class LoginController {

    @GetMapping("")
    public String showLoginForm(HttpServletRequest request) {
        String referrer = request.getHeader("Referer");
        if (referrer == null || referrer.contains("/login") || referrer.contains("/register")) {
            return "login";
        }
        else {
            request.getSession().setAttribute("url_prior_login", referrer);
            return "login";
        }
    }
}
