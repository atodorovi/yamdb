package com.soft.yamdb.controller;

import java.util.Collections;
import java.util.List;

import com.soft.yamdb.mapper.MovieMapper;
import com.soft.yamdb.response.BaseMovieResponse;
import com.soft.yamdb.service.TMDBService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/search")
public class SearchController {
    private TMDBService tmdbService;
    private MovieMapper movieMapper;

    @GetMapping("")
    public String getSearchResults(@RequestParam String title, Model model) {
        List<BaseMovieResponse> movies;

        String sanitisedTitle = title.trim();
        if (sanitisedTitle.isEmpty()) {
            movies = Collections.<BaseMovieResponse>emptyList();
        }
        else {
            movies = tmdbService.searchMovieByTitle(sanitisedTitle);
        }

        model.addAttribute("moviesDto", movies.stream().map(movieMapper::toMovieSearchResultDto).toList());
        model.addAttribute("query", sanitisedTitle);
        return "search";
    }
}
