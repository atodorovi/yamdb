package com.soft.yamdb.controller;

import java.util.List;

import com.soft.yamdb.mapper.MovieMapper;
import com.soft.yamdb.response.BaseMovieResponse;
import com.soft.yamdb.service.TMDBService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping(value = "/")
public class HomeController {
    private TMDBService tmdbService;
    private MovieMapper movieMapper;

    @GetMapping("/")
    public String getHome(Model model) {
        List<BaseMovieResponse> mostPopularMovies = tmdbService.getPopularMovies();
        model.addAttribute("moviesDto",
                mostPopularMovies.stream().limit(12).map(movieMapper::toMoviePosterDto).toList());
        return "home";
    }
}
