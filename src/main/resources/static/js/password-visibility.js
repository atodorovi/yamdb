window.addEventListener('load', function() {
    document.getElementById('password-visibility-btn').addEventListener('click', function() {
        let input = this.previousElementSibling;
        let btn = this;

        input.getAttribute('type')=='text' ? input.setAttribute('type','password') : input.setAttribute('type','text');
        btn.classList.toggle('fa-eye-slash');
        btn.classList.toggle('fa-eye');
    });
});