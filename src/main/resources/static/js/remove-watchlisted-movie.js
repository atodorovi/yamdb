window.addEventListener('load', function() {
    Array.from(document.getElementsByClassName('remove-btn')).forEach(btn => {
        btn.addEventListener('click', function() {
            removeMovie(this.parentElement);
        });
    });
});

async function removeMovie(movie_element) {
    let csrf_token = document.querySelector('meta[name="_csrf"]').content;
    let csrf_header = document.querySelector('meta[name="_csrf_header"]').content;
    let url = window.location.origin + '/watchlist/remove/' + movie_element.dataset.id;
    let response = await fetch(url, { method: 'DELETE', headers: { "Content-Type": "application/json; charset=utf-8", [csrf_header] : csrf_token } })
    if (response.status==200) movie_element.remove();
}