window.addEventListener('load', function() {
    let sort_dropdown = document.getElementById('sort-select');
    if (sort_dropdown) {
        let movie_parent = document.getElementsByClassName("grid")[0];
        sortWatchlist(movie_parent, sort_dropdown.value);

        sort_dropdown.addEventListener('change', function() { 
            sortWatchlist(movie_parent, this.value);
        });
    }
});

function sortChildren(parent, comparator) {
    parent.replaceChildren(...Array.from(parent.children).sort(comparator));
}

function sortWatchlist(parent, sort) {
    switch (sort) {
        case 'az':
            sortChildren(parent, (a,b)=> (a.dataset.title.localeCompare(b.dataset.title)));
            break;
        case 'za':
            sortChildren(parent, (a,b)=> (b.dataset.title.localeCompare(a.dataset.title)));
            break;
        case 'desc':
            sortChildren(parent, (a,b)=> (b.dataset.date - a.dataset.date));
            break;
        default:
            break;
    }
}