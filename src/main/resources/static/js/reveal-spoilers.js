window.addEventListener('load', function() {
    Array.from(document.getElementsByClassName('spoiler-btn')).forEach(btn => {
        btn.addEventListener('click', function() {
            this.previousElementSibling.classList.remove('blur-sm','select-none');
            this.remove();
        });
    });
});