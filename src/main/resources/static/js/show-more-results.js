window.addEventListener('load', function() {
    let search_result_container = document.getElementById('search-result-container');
    let search_results = Array.from(search_result_container.children);

    // hide more than 5 results and create button for showing more results
    if (search_results.length > 5) {
        for (let i = 5; i < search_results.length; i++) {
            search_results[i].style.display = 'none';
        }

        let button_container = document.createElement('div');
        let button = document.createElement('button');

        button_container.appendChild(button);
        search_result_container.appendChild(button_container);

        button_container.classList.add('flex');
        button.classList.add('w-full', 'border-2', 'mt-4', 'p-1');

        button.textContent = 'Show More';
        button.id = 'show-more';
        
        button.addEventListener('click', function() {
            showFiveMoreResults(search_results);
        });
    }
});

let showFiveMoreResults = (function() {
    let currently_displayed_results = 5;
    return function(search_results) {
        for (let i = currently_displayed_results; i < currently_displayed_results+5; i++) {
            if (search_results.length-1===i) {
                search_results[i].style.display = 'block';
                document.getElementById('show-more').parentElement.remove();
                return;
            }
            else search_results[i].style.display = 'block';
        }
        currently_displayed_results += 5;
    }
})();